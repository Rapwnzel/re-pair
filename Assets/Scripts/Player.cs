﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Library;

public class Player : MonoBehaviour
{
    public int materialId;
    public float throwForce;
    public KeyCode buttonMine;
    public KeyCode buttonThrow;

    [Header("Cooldowns")]
    public float mineSpeed = 0.1f;    // how much mining is done per FixedUpdate (1/50 per sec)

    [Header("References")]
    public GameObject throwBox;
    public GameObject forceBox;
    public MeteorQueue meteorQueue;

    private GameObject meteor;

    // controls
    private bool ctrl_mine;
    private bool ctrl_throw;
    
    // states
    private float throwState;   // 0 = not throwing, 1 = angle selection, 2 = force selection
    private float currentAngle;
    private float currentForce;

    private AudioSource asrc;
    
	void Start () {
		throwState = 0;
        transform.Find("laser").GetComponent<SpriteRenderer>().color = materialId == 0 ? Color.green : Color.blue;
        asrc = gameObject.GetComponent<AudioSource>();
    }
	
	void Update () {
		ctrl_mine = Input.GetKey(buttonMine);
        if (Input.GetKeyDown(buttonThrow)){
            ctrl_throw = true;
        }
	}

    void FixedUpdate() {
        if (!Manager.levelDone)
        {
            CheckMine();
            CheckThrow();
        }
    }

    private void CheckMine(){
        if(ctrl_mine && throwState == 0){
            ApplyMaterial();
        } else if(ctrl_mine && throwState != 0){
            throwState = 3;
        } else {
            transform.Find("laser").gameObject.SetActive(false);
        }
    }

    private void CheckThrow(){
        if(ctrl_throw || throwState == 3){
            switch(throwState){
                case 0: 
                    if(!meteorQueue.queueIsEmpty()){
                        meteor = meteorQueue.FirstMeteor();
                        throwBox.SetActive(true);
                        throwState++;
                    } else {
                        // TODO: add sound or some effect
                    }
                    
                    break;
                case 1: 
                    currentAngle = throwBox.GetComponent<AngleSelection>().GetCurrentAngle();
                    throwBox.GetComponent<AngleSelection>().StopArrow();
                    forceBox.SetActive(true);
                    throwState++;
                    break;
                case 2: 
                    // AudioClip
                    asrc.Play();
                    throwBox.SetActive(false);
                    currentForce = forceBox.GetComponent<ForceSelection>().GetCurrentPercent() * throwForce;
                    forceBox.SetActive(false);
                    throwBox.GetComponent<AngleSelection>().ResetArrow();
                    forceBox.GetComponent<ForceSelection>().ResetBar();
                    Launch(currentAngle,currentForce);
                    meteorQueue.DequeueMeteor();
                    throwState = 0;
                break;
                case 3:
                    // abort because player wants to mine
                    throwBox.SetActive(false);
                    forceBox.SetActive(false);
                    throwBox.GetComponent<AngleSelection>().ResetArrow();
                    forceBox.GetComponent<ForceSelection>().ResetBar();
                    throwState = 0;
                break;
            }
        }
        ctrl_throw=false;
    }

    private void ApplyMaterial(){
        GameObject first = meteorQueue.FirstMeteor();
        if (first!=null)
        {
            first.GetComponent<Meteorite>().UpdateMaterial(materialId, mineSpeed);
            transform.Find("laser").gameObject.SetActive(true);
        }
    }

    public void Launch(float angle, float force)
    {
        if(meteor!=null){
            meteor.transform.position = transform.position + new Vector3(0,0,-1);
            //meteor.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            meteor.layer = LayerMask.NameToLayer("MeteorThrow");
            Rigidbody2D rig2D = meteor.GetComponent<Rigidbody2D>();
            float angleInRad = Mathf.Deg2Rad * angle;
            Vector2 thrustVector = new Vector2(Mathf.Cos(angleInRad), Mathf.Sin(angleInRad));
            rig2D.AddForce(thrustVector * force * meteor.GetComponent<Rigidbody2D>().mass, ForceMode2D.Impulse);
            //rig2D.AddTorque(10, ForceMode2D.Force);
            rig2D.angularVelocity = Random.Range(.1f, 300f);
            meteor=null;
        }
    }
}
