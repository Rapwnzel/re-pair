﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceSelection : MonoBehaviour
{
    public float barSpeed = 0.01f;
    public float barMin = 0f;
    public float barMax = 1f;
    public GameObject bar;

    private float currentScale;
    private int barChangeDirection;

    void Start()
    {
        ResetBar();
    }

    void FixedUpdate()
    {
        currentScale = currentScale + barSpeed * barChangeDirection;
        if(currentScale >= barMax){
            barChangeDirection = -1;
        } else if(currentScale <= barMin){
            barChangeDirection = 1;
        }
        // arrow.transform.rotation = Quaternion.Euler(0, 0, currentScale);
        bar.transform.localScale = new Vector3(currentScale, bar.transform.localScale.y, 1);
    }

    // returns current force in percent (between 0 and 1)
    public float GetCurrentPercent(){
        return currentScale;
    }

    public void ResetBar(){
        currentScale = 0;
        bar.transform.localScale = new Vector3(currentScale, bar.transform.localScale.y, 1);
        barChangeDirection = 1;
    }
}
