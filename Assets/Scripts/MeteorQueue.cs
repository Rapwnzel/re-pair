﻿using System.Collections.Generic;
using System.Linq;
using Library;
using UnityEngine;

public class MeteorQueue : MonoBehaviour
{
    private Queue<GameObject> queue;
    private Material defaultMaterial;
    private Material highlightMaterial;
    public QueueStatus status;
    public Transform spawnPoint;
    public bool rightSide;

    public GameObject meteor;
    public GameObject teleportEndEffect;
    public GameObject firstBefore;

    GameObject toAdd;

    void Start(){
        queue = new Queue<GameObject>();
        defaultMaterial = Resources.Load<Material>("Materials/Outline");
        highlightMaterial = Resources.Load<Material>("Materials/Highlighted");
    }

    void Update(){
        if (toAdd!=null)
        {
            toAdd.transform.position = spawnPoint.position;
            toAdd.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            toAdd.GetComponent<Rigidbody2D>().angularVelocity=0;
            Instantiate(teleportEndEffect, spawnPoint.position, Quaternion.identity);
            toAdd = null;
        }

        if (queue.Count > 0 && (firstBefore == null || queue.First() != firstBefore))
        {
            queue.First().GetComponent<MeshRenderer>().material = highlightMaterial;
            firstBefore = queue.First();
        }
    }


    public GameObject SpawnMeteor(int numberTypes, int maxTotal){
        int cap = Random.Range(0, maxTotal);
        if (numberTypes==0||cap==0)
        {
            return SpawnMeteor(new int[]{});
        }
        int[] minerals = new int[cap];
        int[] possibleMinerals = rightSide? new int[]{1,0}: new int[]{0,1};
        for (int i = 0; i < cap; i++)
        {
            minerals[i] = possibleMinerals[Random.Range(0,numberTypes)];
        }
        return SpawnMeteor(minerals);
    }

    public GameObject SpawnMeteor(int[] minerals){
        GameObject m = null;
        if(canSpawnMeteor()){
            m = Instantiate(meteor, spawnPoint.position,Quaternion.identity);
            m.tag = "Meteor";
            m.layer = LayerMask.NameToLayer("MeteorQueue");
            m.GetComponent<Meteorite>().initialize(rightSide, minerals);
            m.GetComponent<MeshRenderer>().material = defaultMaterial;
            m.GetComponent<Meteorite>().rightSide = rightSide;
        }
        return m;
    }

    public void AddExisting(GameObject meteor){
        toAdd = meteor;
    }

    void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.layer != LayerMask.NameToLayer("MeteorQueue"))
        {
            return;
        }
        foreach (var item in queue){
            if (item == other.gameObject){
                return;
            }
        }
        queue.Enqueue(other.gameObject);
    }

    public GameObject DequeueMeteor()
    {
        GameObject go = queue.Dequeue();
        go.GetComponent<MeshRenderer>().material = defaultMaterial;
        return go;
    }

    public GameObject FirstMeteor(){
        if (queue.Count>0){
            return queue.First();
        }
        return null;
    }

    public bool queueIsEmpty(){
        return queue.Count==0;
    }

    public bool canSpawnMeteor(){
        if (toAdd!=null)
        {
            return false;
        }
        return !status.isFull();
    }

    public List<GameObject> ToList()
    {
        List<GameObject> goa = new List<GameObject>(queue.Count);
        foreach (var go in queue)
        {
            goa.Add(go);
        }
        return goa;
    }
}
