/* 
 * Convex hull algorithm - Library (C#)
 * 
 * Copyright (c) 2017 Project Nayuki
 * https://www.nayuki.io/page/convex-hull-algorithm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program (see COPYING.txt and COPYING.LESSER.txt).
 * If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ConvexHull
{
    public static IList<Vector2> V32V2(IList<Vector3> v3)
    {
        return v3.Select(v => new Vector2(v.x, v.y)).ToList();
    }

    // Returns a new list of points representing the convex hull of
    // the given set of points. The convex hull excludes collinear points.
    // This algorithm runs in O(n log n) time.
    public static IList<Vector3> MakeHull(IList<Vector3> points)
    {
        List<Vector3> newPoints = new List<Vector3>(points);
        newPoints.Sort((a, b) => Vector3.SqrMagnitude(a - b) < 0.0001 ? -1 : 1);
        return MakeHullPresorted(newPoints);
    }


    // Returns the convex hull, assuming that each points[i] <= points[i + 1]. Runs in O(n) time.
    public static IList<Vector3> MakeHullPresorted(IList<Vector3> points)
    {
        if (points.Count <= 1)
            return new List<Vector3>(points);

        // Andrew's monotone chain algorithm. Positive y coordinates correspond to "up"
        // as per the mathematical convention, instead of "down" as per the computer
        // graphics convention. This doesn't affect the correctness of the result.

        List<Vector3> upperHull = new List<Vector3>();
        foreach (Vector3 p in points)
        {
            while (upperHull.Count >= 2)
            {
                Vector3 q = upperHull[upperHull.Count - 1];
                Vector3 r = upperHull[upperHull.Count - 2];
                if ((q.x - r.x) * (p.y - r.y) >= (q.y - r.y) * (p.x - r.x))
                    upperHull.RemoveAt(upperHull.Count - 1);
                else
                    break;
            }

            upperHull.Add(p);
        }

        upperHull.RemoveAt(upperHull.Count - 1);

        IList<Vector3> lowerHull = new List<Vector3>();
        for (int i = points.Count - 1; i >= 0; i--)
        {
            Vector3 p = points[i];
            while (lowerHull.Count >= 2)
            {
                Vector3 q = lowerHull[lowerHull.Count - 1];
                Vector3 r = lowerHull[lowerHull.Count - 2];
                if ((q.x - r.x) * (p.y - r.y) >= (q.y - r.y) * (p.x - r.x))
                    lowerHull.RemoveAt(lowerHull.Count - 1);
                else
                    break;
            }

            lowerHull.Add(p);
        }

        lowerHull.RemoveAt(lowerHull.Count - 1);

        if (!(upperHull.Count == 1 && upperHull.SequenceEqual(lowerHull)))
            upperHull.AddRange(lowerHull);
        return upperHull;
    }
}