﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Manager : MonoBehaviour
{
    public MeteorQueue leftQueue;
    public MeteorQueue rightQueue;
    public float startFirstMineralAt = 60;
    public float startSecondMineralAt = 300;
    public float spawnRateMin = 3f;
    public float spawnRateMax = 10f;
    private float spawnTimeLeft = 0;
    private float spawnTimeRight = 0;

    public float timer;
    public Text pausedText;
    public Text uiTimer;
    public Text victoryText;
    public Button victoryButton;
    public Button restartButton;
    public Button quitButton;
    public GameObject bubbles;

    public static HashSet<GameObject> connectedMeteors;
    
    public static bool levelDone;
    private bool paused;
 
    void Start(){
        Application.targetFrameRate = 144;
        connectedMeteors = new HashSet<GameObject>();
    }

    public void Quit(){
        Application.Quit();
    }

    void Update()
    {
        if (levelDone)
        {
            MakeBridgeHull();
            victoryText.gameObject.SetActive(true);
            victoryButton.gameObject.SetActive(true);
            bubbles.gameObject.SetActive(true);
            SpriteRenderer[] sr = bubbles.GetComponentsInChildren<SpriteRenderer>();
            sr.ToList().ForEach(r => r.transform.localEulerAngles = new Vector3(0f, 0f, Random.Range(-5f, 5f)));
            quitButton.gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (paused){
                paused=false;
                Time.timeScale = 1;
                pausedText.gameObject.SetActive(false);
                restartButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(false);
            }else{
                paused=true;
                Time.timeScale = 0;
                pausedText.gameObject.SetActive(true);
                restartButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(true);
            }
        }

        if (!paused){    
            UpdateTime();
            UpdateUI();
            UpdateSpawn();
        }
    }

    public void ResetGame(){
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
        victoryText.gameObject.SetActive(false);
        victoryButton.gameObject.SetActive(false);
        bubbles.gameObject.SetActive(false);
        paused=false;
        Time.timeScale = 1;
        levelDone=false;
    }

    private void UpdateTime(){
        timer += Time.deltaTime;
        if(timer <= 0){
            timer = 0;
        }
    }

    private void UpdateUI(){
        if (!levelDone)
        {
            int seconds = Mathf.FloorToInt(timer);
            int ms = Mathf.RoundToInt((timer-seconds)*100);
            uiTimer.text = string.Format("{0}.{1:00}", seconds, ms);
        }
    }

    private void UpdateSpawn(){
        int mineralTypes = 0;
        if (timer>startFirstMineralAt){
            mineralTypes++;
            if (timer>startSecondMineralAt){
                mineralTypes++;
            }
        }
        spawnTimeLeft-=Time.deltaTime;
        if (spawnTimeLeft<=0&&leftQueue.canSpawnMeteor())
        {
            spawnTimeLeft = Random.Range(spawnRateMin,spawnRateMax);
            leftQueue.SpawnMeteor(mineralTypes, 4);
        }

        spawnTimeRight-=Time.deltaTime;
        if (spawnTimeRight<=0&&rightQueue.canSpawnMeteor())
        {
            spawnTimeRight = Random.Range(spawnRateMin,spawnRateMax);
            rightQueue.SpawnMeteor(mineralTypes, 4);
        }
    }
    public GameObject FindObject(string name)
    {
         Transform[] trs= gameObject.GetComponentsInChildren<Transform>(true);
         foreach(Transform t in trs){
             if(t.name == name){
                  return t.gameObject;
             }
         }
         return null;
    }

    public static void setLevelDone() {
        levelDone = true;
        Material pinky = Resources.Load<Material>("Materials/Rosa");
        foreach (var meteor in connectedMeteors)
        {
            if (meteor != null) meteor.GetComponent<MeshRenderer>().material = pinky;
        }
    }

    public void MakeBridgeHull()
    {
        IList<Vector3> inVerts = connectedMeteors.ToList()
            .Where(m => m != null)
            .Select(m => m.GetComponent<MeshFilter>().mesh.vertices
                .Select(v => m.transform.TransformPoint(v)))
            .SelectMany(x => x).Distinct().ToList();

        IList<Vector3> hull = GetConvexHull(inVerts.ToList());
        Debug.Log("Hull is: " + hull.Count);

        

        //sort hull verts
        Vector3 hullCenter = Vector3.zero;
        foreach (var item in hull){
            hullCenter += item;
        }
        hullCenter /= hull.Count;
        Debug.Log(hullCenter);

        hull = hull.OrderBy(v => Mathf.Atan2(v.y-hullCenter.y,v.x-hullCenter.x)).ToList();

        Mesh outer = Meteorite.GenerateMeshFromVertices(hull);
        GameObject go = new GameObject();
        go.AddComponent<MeshFilter>().mesh = outer;
        go.AddComponent<OutlineCreator>();
    }

    private static List<Vector3> GetConvexHull(List<Vector3> points)
    {
        //If we have just 3 points, then they are the convex hull, so return those
        if (points.Count == 3)
        {
            //These might not be ccw, and they may also be colinear
            return points;
        }

        //If fewer points, then we cant create a convex hull
        if (points.Count < 3)
        {
            return null;
        }



        //The list with points on the convex hull
        List<Vector3> convexHull = new List<Vector3>();

        //Step 1. Find the vertex with the smallest x coordinate
        //If several have the same x coordinate, find the one with the smallest z
        Vector3 startVertex = points[0];

        Vector3 startPos = startVertex;

        for (int i = 1; i < points.Count; i++)
        {
            Vector3 testPos = points[i];

            //Because of precision issues, we use Mathf.Approximately to test if the x positions are the same
            if (testPos.x < startPos.x || (Mathf.Approximately(testPos.x, startPos.x) && testPos.z < startPos.z))
            {
                startVertex = points[i];

                startPos = startVertex;
            }
        }

        //This vertex is always on the convex hull
        convexHull.Add(startVertex);

        points.Remove(startVertex);

      

        //Step 2. Loop to generate the convex hull
        Vector3 currentPoint = convexHull[0];

        //Store colinear points here - better to create this list once than each loop
        List<Vector3> colinearPoints = new List<Vector3>();

        int counter = 0;

        while (true)
        {
            //After 2 iterations we have to add the start position again so we can terminate the algorithm
            //Cant use convexhull.count because of colinear points, so we need a counter
            if (counter == 2)
            {            
                points.Add(convexHull[0]);
            }
        
            //Pick next point randomly
            Vector3 nextPoint = points[Random.Range(0, points.Count)];

            //To 2d space so we can see if a point is to the left is the vector ab
            Vector2 a = new Vector2(currentPoint.x,currentPoint.y);

            Vector2 b = new Vector2(nextPoint.x,nextPoint.y);

            //Test if there's a point to the right of ab, if so then it's the new b
            for (int i = 0; i < points.Count; i++)
            {
                //Dont test the point we picked randomly
                if (points[i].Equals(nextPoint))
                {
                    continue;
                }
            
                Vector2 c = new Vector2(points[i].x,points[i].y);

                //Where is c in relation to a-b
                // < 0 -> to the right
                // = 0 -> on the line
                // > 0 -> to the left
                Vector3 direction = a-b;
                Vector3 startingPoint = b;
                Vector3 vc = new Vector3(c.x,c.y);
                Ray ray = new Ray(startingPoint, direction);
                float distance = Vector3.Cross(ray.direction, vc - ray.origin).magnitude;

                Vector2 perp = new Vector2(-(a-b).y,(a-b).x);
                float dot = Vector2.Dot(perp, c-b);
                float relation = dot<0?-distance:(dot>0?distance:0);
                //float relation = Geometry.IsAPointLeftOfVectorOrOnTheLine(a, b, c);
                
                //Colinear points
                //Cant use exactly 0 because of floating point precision issues
                //This accuracy is smallest possible, if smaller points will be missed if we are testing with a plane
                float accuracy = 0.00001f;

                if (relation < accuracy && relation > -accuracy)
                {
                    colinearPoints.Add(points[i]);
                }
                //To the right = better point, so pick it as next point on the convex hull
                else if (relation < 0f)
                {
                    nextPoint = points[i];

                    b = new Vector2(nextPoint.x,nextPoint.y);

                    //Clear colinear points
                    colinearPoints.Clear();
                }
                //To the left = worse point so do nothing
            }

        

            //If we have colinear points
            if (colinearPoints.Count > 0)
            {
                colinearPoints.Add(nextPoint);

                //Sort this list, so we can add the colinear points in correct order
                colinearPoints = colinearPoints.OrderBy(n => Vector3.SqrMagnitude(n - currentPoint)).ToList();

                convexHull.AddRange(colinearPoints);

                currentPoint = colinearPoints[colinearPoints.Count - 1];

                //Remove the points that are now on the convex hull
                for (int i = 0; i < colinearPoints.Count; i++)
                {
                    points.Remove(colinearPoints[i]);
                }

                colinearPoints.Clear();
            }
            else
            {
                convexHull.Add(nextPoint);
            
                points.Remove(nextPoint);

                currentPoint = nextPoint;
            }

            //Have we found the first point on the hull? If so we have completed the hull
            if (currentPoint.Equals(convexHull[0]))
            {
                //Then remove it because it is the same as the first point, and we want a convex hull with no duplicates
                convexHull.RemoveAt(convexHull.Count - 1);

                break;
            }

            counter += 1;
        }

        return convexHull;
    }
}
