﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject instructionsPanel;

    public void StartGame(){
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void QuitGame(){
        Application.Quit();
    }

    public void ToggleInstructions(bool on){
        instructionsPanel.SetActive(on);
    }
}
