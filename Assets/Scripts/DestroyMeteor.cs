﻿using System.Collections;
using System.Collections.Generic;
using Library;
using UnityEngine;

public class DestroyMeteor : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D.gameObject.GetComponent<Meteorite>())
        {
            if (gameObject.CompareTag("Explode"))
            {
                Explode(collider2D.gameObject);
            }
            else
            {
                Destroy(collider2D.gameObject);
            }
        }

        void Explode(GameObject meteor)
        {
            GameObject exp = Instantiate(Resources.Load<GameObject>("Particles/MakeBoom"),
                meteor.transform.position,
                meteor.transform.rotation
            );
            Destroy(meteor);
            Destroy(exp, .5f);
        }
    }
}