﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Library;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour
{
    private float elapsed;
    private Material mat;

    public GameObject meteor;

    public float spawnCD = 1f;

    public Transform leftSpawn;
    public Transform rightSpawn;
    // Start is called before the first frame update
    void Start()
    {
        elapsed = spawnCD;
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= spawnCD) {
            elapsed = elapsed % spawnCD;

            float spawnX = Random.Range(leftSpawn.position.x, rightSpawn.position.x);
            GameObject m = Instantiate(meteor, new Vector3(spawnX, (leftSpawn.position.y+rightSpawn.position.y)/2,1), Quaternion.identity);
            m.GetComponent<Renderer>().material = Resources.Load<Material>("Materials/BGMeteor");
            m.layer = LayerMask.NameToLayer("MeteorBG");
            m.tag = "Meteor";
            m.GetComponent<Rigidbody2D>().drag = Random.Range(1f,2f);
            m.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-100f,100f);
        }
    }
}
