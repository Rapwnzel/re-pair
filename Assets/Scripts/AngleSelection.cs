﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleSelection : MonoBehaviour
{
    public float arrowSpeed = 0.1f;
    public float angleMin = 0.1f;
    public float angleMax = 89.9f;
    public float startAngle = 0f;
    public GameObject arrow;

    private bool move;
    private float currentAngle;
    private int angleChangeDirection;

    void Start()
    {
        ResetArrow();
    }

    void FixedUpdate()
    {
        if(move){
            currentAngle = currentAngle + arrowSpeed * angleChangeDirection;
            if(currentAngle >= angleMax){
                angleChangeDirection = -1;
            } else if(currentAngle <= angleMin){
                angleChangeDirection = 1;
            }
            arrow.transform.rotation = Quaternion.Euler(0, 0, currentAngle);
        }
    }

    public float GetCurrentAngle(){
        return currentAngle;
    }

    public void StopArrow(){
        move = false;
    }

    public void ResetArrow(){
        currentAngle = startAngle;
        arrow.transform.rotation = Quaternion.Euler(0, 0, currentAngle);
        angleChangeDirection = 1;
        move = true;
    }
}
