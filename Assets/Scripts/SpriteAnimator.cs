﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAnimator : MonoBehaviour
{
    public Sprite[] sprites;
    public float timePerSprite;
    public int startSprite = 0;

    private int currentSprite;
    private float lastChange;

    void Start()
    {
        currentSprite = startSprite;
    }

    void FixedUpdate()
    {
        if(Time.time > lastChange + timePerSprite){
            currentSprite = (currentSprite + 1) % sprites.Length;
            GetComponent<Image>().sprite = sprites[currentSprite];
            lastChange = Time.time;
        }
    }
}
