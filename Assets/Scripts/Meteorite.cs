using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Library
{
    public class Mineral
    {
        public int Type;
        public double Percent = 0;
        public GameObject mineralUI;

        public Mineral(int type)
        {
            Type = type;
        }
    }

    public class Meteorite : MonoBehaviour
    {
        public float weight;
        public List<Mineral> minerals;
        public GameObject mineralSprite;
        public GameObject mineralInfusedEffect;
        public bool rightSide;
        bool connected = false;

        public const int MIN_VERTICES = 5;
        public const int MAX_VERTICES = 12;

        public float minSize = 0.5f;
        public float maxSize = 1.0f;

        private AudioSource asrc;
        private AudioSource stickAudio;

        public void Start()
        {
            Random.InitState(Guid.NewGuid().GetHashCode());
            int verts = Random.Range(MIN_VERTICES, MAX_VERTICES + 1);
            List<Vector3> verticesList = generateRandomConvexPolygon(verts);

            Mesh msh = GenerateMeshFromVertices(verticesList);
            msh.name = "MeteorMesh";
            // Set up game object with mesh;
            GetComponent<MeshFilter>().mesh = msh;

            // Physics!
            PolygonColliderHelper.UpdatePolygonCollider2D(GetComponent<MeshFilter>());
            gameObject.GetComponent<Rigidbody2D>().mass = gameObject.GetComponent<Meteorite>().weight;

            asrc = transform.Find("Audio1").gameObject.GetComponent<AudioSource>();
            stickAudio = GetComponent<AudioSource>();
        }

        public static Mesh GenerateMeshFromVertices(IList<Vector3> verticesList)
        {
            int[] indices = new int[verticesList.Count * 3];
            for (int i = 0; i < verticesList.Count - 1; i++)
            {
                indices[i * 3] = 0;
                indices[i * 3 + 1] = i;
                indices[i * 3 + 2] = i + 1;
            }

            // Create the mesh
            Mesh msh = new Mesh();
            msh.vertices = verticesList.ToArray();
            msh.triangles = indices;
            msh.RecalculateNormals();
            msh.RecalculateBounds();
            return msh;
        }

        public void initialize(bool rightSide, Mineral[] minerals)
        {
            this.rightSide = rightSide;
            this.minerals = new List<Mineral>(minerals);
            CreateMinerals();
        }

        public void initialize(bool rightSide, int[] minerals)
        {
            this.rightSide = rightSide;
            this.minerals = new List<Mineral>();
            foreach (var mineralSingle in minerals)
            {
                this.minerals.Add(new Mineral(mineralSingle));
            }

            CreateMinerals();
        }

        private void CreateMinerals()
        {
            Vector3[] locations =
            {
                new Vector3(transform.position.x - 0.1f, transform.position.y - 0.1f, transform.position.z - 1),
                new Vector3(transform.position.x + 0.1f, transform.position.y - 0.1f, transform.position.z - 1),
                new Vector3(transform.position.x - 0.1f, transform.position.y + 0.1f, transform.position.z - 1),
                new Vector3(transform.position.x + 0.1f, transform.position.y + 0.1f, transform.position.z - 1),
            };
            int locCounter = 0;
            // for each mineral
            foreach (var mineral in minerals)
            {
                // add at free position as child of this gameObject
                GameObject minObj = Instantiate(mineralSprite, locations[locCounter], Quaternion.identity);
                mineral.mineralUI = minObj;
                minObj.transform.SetParent(gameObject.transform);
                minObj.GetComponent<SpriteRenderer>().color = mineral.Type == 1 ? Color.blue : Color.green;
                locCounter++;
            }
        }

        public bool IsSticky()
        {
            bool sticky = true;
            foreach (var mineral in minerals)
            {
                if (mineral.Percent < 100)
                {
                    sticky = false;
                }
            }

            return sticky;
        }

        // called by player every Update when mining
        public void UpdateMaterial(int typePlayer, float percent)
        {
            foreach (var mineral in minerals)
            {
                if (mineral.Type == typePlayer && mineral.Percent < 100)
                {
                    mineral.Percent += percent;
                    if (mineral.Percent >= 100)
                    {
                        mineral.Percent = 100;
                        Instantiate(mineralInfusedEffect, mineral.mineralUI.transform.position, Quaternion.identity);
                        Destroy(mineral.mineralUI);
                        asrc.Play();
                    }

                    break;
                }
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (gameObject.layer == LayerMask.NameToLayer("MeteorQueue")) return;
            if (!IsSticky()) return;
            if (collision.gameObject.GetComponent<Attachment>() == null)
            {
                Meteorite stickyMet = collision.gameObject.GetComponent<Meteorite>();
                if (stickyMet == null || !stickyMet.IsSticky())
                {
                    return;
                }
            }

            GameObject goB = collision.gameObject;
            if (connected)
            {
                Meteorite otherMet = goB.GetComponent<Meteorite>();

                // if also the other one is connected, bridge is complete
                if (otherMet != null && otherMet.connected && otherMet.rightSide != rightSide)
                {
                    Manager.connectedMeteors.Add(goB);
                    Manager.connectedMeteors.Add(gameObject);
                    Manager.setLevelDone();
                }
                else
                {
                    return;
                }
            }

            if (gameObject.layer == LayerMask.NameToLayer("MeteorThrow") &&
                goB.layer == LayerMask.NameToLayer("MeteorThrow"))
            {
                // AudioClip
                stickAudio.Play();
                FixedJoint2D[] jsOld = gameObject.GetComponents<FixedJoint2D>();
                if (jsOld.Length > 0)
                {
                    foreach (var jnt in jsOld)
                    {
                        if (jnt.connectedBody == goB.GetComponent<Rigidbody2D>()) return;
                    }
                }

                Attachment att = goB.GetComponent<Attachment>();
                if (att != null)
                {
                    rightSide = att.rightSide;
                }
                else
                {
                    rightSide = goB.GetComponent<Meteorite>().rightSide;
                }

                Material sh = Resources.Load<Material>("Materials/Connected" + (rightSide ? "Right" : "Left"));
                gameObject.GetComponent<MeshRenderer>().material = sh;
                connected = true;

                FixedJoint2D js = goB.AddComponent<FixedJoint2D>();
                js.connectedBody = gameObject.GetComponent<Rigidbody2D>();
                //js.autoConfigureDistance = true;
                js.dampingRatio = .3f;
                Manager.connectedMeteors.Add(gameObject);
                
            }
        }

        private static float calcWeight(List<Vector3> verticesList, int[] triangles)
        {
            float weight = 0;
            for (int i = 0; i < triangles.Length; i += 3)
            {
                weight += (
                              verticesList[triangles[i]].x *
                              Math.Abs(verticesList[triangles[i + 1]].y - verticesList[triangles[i + 2]].y) +
                              verticesList[triangles[i + 1]].x *
                              Math.Abs(verticesList[triangles[i + 2]].y - verticesList[triangles[i]].y) +
                              verticesList[triangles[i + 2]].x *
                              Math.Abs(verticesList[triangles[i]].y - verticesList[triangles[i + 1]].y)
                          ) / 2f;
            }

            return weight;
        }

        private static List<Vector3> generateRandomConvexPolygon(int n)
        {
            // Generate two lists of float X and Y coordinates
            List<float> xPool = new List<float>(n);
            List<float> yPool = new List<float>(n);

            for (int i = 0; i < n; i++)
            {
                xPool.Add(Random.value);
                yPool.Add(Random.value);
            }

            // Sort them
            xPool.Sort();
            yPool.Sort();

            // Isolate the extreme points
            float minX = xPool[0];
            float maxX = xPool[n - 1];
            float minY = yPool[0];
            float maxY = yPool[n - 1];

            // Divide the interior points into two chains & Extract the vector components
            List<float> xVec = new List<float>(n);
            List<float> yVec = new List<float>(n);

            float lastTop = minX, lastBot = minX;

            float x;
            for (int i = 1; i < n - 1; i++)
            {
                x = xPool[i];
                if (Random.value > .5)
                {
                    xVec.Add(x - lastTop);
                    lastTop = x;
                }
                else
                {
                    xVec.Add(lastBot - x);
                    lastBot = x;
                }
            }

            xVec.Add(maxX - lastTop);
            xVec.Add(lastBot - maxX);

            float lastLeft = minY, lastRight = minY;

            float y;
            for (int i = 1; i < n - 1; i++)
            {
                y = yPool[i];
                if (Random.value > .5)
                {
                    yVec.Add(y - lastLeft);
                    lastLeft = y;
                }
                else
                {
                    yVec.Add(lastRight - y);
                    lastRight = y;
                }
            }

            yVec.Add(maxY - lastLeft);
            yVec.Add(lastRight - maxY);

            // Randomly pair up the X- and Y-components
            for (int t = 0; t < yVec.Count; t++)
            {
                float tmp = yVec[t];
                int r = Random.Range(t, yVec.Count);
                yVec[t] = yVec[r];
                yVec[r] = tmp;
            }

            // Combine the paired up components into vectors
            List<Vector2> vec = new List<Vector2>();

            for (int i = 0; i < n; i++)
            {
                vec.Add(new Vector2(xVec[i], yVec[i]));
            }

            // Sort the vectors by angle
            vec.Sort((a, b) => Mathf.Atan2(b.y, b.x) - Mathf.Atan2(a.y, a.x) <= 0 ? -1 : 1);

            // Lay them end-to-end
            x = 0;
            y = 0;
            float minPolygonX = 0;
            float minPolygonY = 0;
            List<Vector3> points = new List<Vector3>();

            for (int i = 0; i < n; i++)
            {
                points.Add(new Vector3(x, y, 0));

                x += vec[i].x;
                y += vec[i].y;

                minPolygonX = Mathf.Min(minPolygonX, x);
                minPolygonY = Mathf.Min(minPolygonY, y);
            }

            // Move the polygon to the original min and max coordinates
            Vector3 size = new Vector3(maxX - minX, maxY - minY);
            float xShift = minPolygonX + size.x / 2;
            float yShift = minPolygonY + size.y / 2;

            for (int i = 0; i < n; i++)
            {
                Vector3 p = points[i] / (Mathf.Max(size.x, size.y));
                points[i] = new Vector3(p.x - xShift, p.y - yShift, 0);
            }

            return points;
        }
    }
}