﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIToggler : MonoBehaviour
{
    public GameObject[] elements;
    public float timePerElement;
    public int startElement = 0;

    private int currentElement;
    private float lastChange;

    void Start()
    {
        currentElement = startElement;
    }

    void FixedUpdate()
    {
        if(Time.time > lastChange + timePerElement){
            foreach (var go in elements)
            {
                go.SetActive(false);
            }
            currentElement = (currentElement + 1) % elements.Length;
            elements[currentElement].SetActive(true);
            lastChange = Time.time;
        }
    }
}
