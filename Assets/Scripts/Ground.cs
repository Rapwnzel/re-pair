﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Library;

public class Ground : MonoBehaviour
{
    public bool rightSide;
    public MeteorQueue queue;

    public GameObject teleportStartEffect;
    
    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.layer == LayerMask.NameToLayer("MeteorThrow")&&collision.gameObject.tag == "Meteor"){
            bool otherRightSide = collision.gameObject.GetComponent<Meteorite>().rightSide;
            if (rightSide^otherRightSide)
            {
                Instantiate(teleportStartEffect, collision.gameObject.transform.position, Quaternion.identity);
                collision.gameObject.layer = LayerMask.NameToLayer("MeteorQueue");
                collision.gameObject.GetComponent<Meteorite>().rightSide = rightSide;
                queue.AddExisting(collision.gameObject);
                GetComponent<AudioSource>().Play();
            }else{
                Destroy(collision.gameObject);
            }
        }
    }
}
