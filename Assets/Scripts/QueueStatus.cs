﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueStatus : MonoBehaviour
{
    int counter = 0;

    void OnTriggerEnter2D(){
        counter++;
    }

    void OnTriggerExit2D(){
        counter--;
    }

    public bool isFull(){
        return counter>0;
    }
}
